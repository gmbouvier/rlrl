use std::fmt;

extern crate rustyline;
use rustyline::error::ReadlineError;
use rustyline::Editor;

#[macro_use]
extern crate combine;
use combine::error::ParseError;
use combine::parser::char::{char, digit, spaces};
use combine::stream::Stream;
use combine::{between, choice, many1, one_of, parser, sep_by, Parser};

// Can a trait object be used for RlExp?
// Kind/Prim not good names.
// Can constructors be genericised?
#[derive(Clone, Debug)]
pub enum RlExp {
    Error(String),
    Symbol(String),
    Int(i64),
    Sexpr(Vec<RlExp>),
}

impl fmt::Display for RlExp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            RlExp::Sexpr(sexpr) => {
                let sexpr_str = sexpr
                    .into_iter()
                    .map(|exp| exp.to_string())
                    .collect::<Vec<String>>()
                    .join(" ");
                return write!(f, "({})", sexpr_str);
            }
            _ => return write!(f, "{}", self.to_string()),
        }
    }
}

impl RlExp {
    fn error(s: &str) -> RlExp {
        RlExp::Error(s.to_string())
    }

    fn int(i: i64) -> RlExp {
        RlExp::Int(i)
    }

    fn sym(s: String) -> RlExp {
        RlExp::Symbol(s.to_string())
    }

    fn sexpr(v: Vec<RlExp>) -> RlExp {
        RlExp::Sexpr(v)
    }

    fn to_string(&self) -> String {
        match &self {
            RlExp::Int(n) => n.to_string(),
            RlExp::Symbol(s) => format!("\"{}\"", s.to_string()),
            RlExp::Error(s) => format!("Error: {}", s.to_string()),
            RlExp::Sexpr(sexpr) => sexpr
                .into_iter()
                .map(|lval| lval.to_string())
                .collect::<Vec<String>>()
                .join(" "),
        }
    }

    fn eval(&self) -> RlExp {
        match self {
            RlExp::Sexpr(sexpr) => {
                let sym = match sexpr.first() {
                    Some(sym) => sym,
                    None => return RlExp::error("empty sexpr"),
                };
                let partial = sexpr
                    .iter()
                    .skip(1)
                    .map(|exp| exp.eval())
                    .collect::<Vec<RlExp>>();
                match sym {
                    RlExp::Symbol(sym) => match sym.as_ref() {
                        "+" => rlsum(&partial),
                        "-" => rlsub(&partial),
                        "*" => rlmul(&partial),
                        "/" => rldiv(&partial),
                        _ => RlExp::error("Operation not supported"),
                    },
                    _ => RlExp::error("0th expr is not a sym"),
                }
            }
            _ => self.clone(),
        }
    }
}

pub fn rlsum(args: &Vec<RlExp>) -> RlExp {
    match args.len() {
        0 => RlExp::error("No args to rlsum"),
        _ => {
            let mut result = 0;
            for exp in args {
                match exp {
                    RlExp::Int(i) => result += i,
                    RlExp::Error(_) => return exp.clone(),
                    _ => return RlExp::error("An arg to rlsum did not eval to an int"),
                }
            }

            return RlExp::int(result);
        }
    }
}

pub fn rlsub(args: &Vec<RlExp>) -> RlExp {
    let first_arg = match args.first() {
        Some(exp) => match exp {
            RlExp::Int(i) => i,
            RlExp::Error(_) => return exp.clone(),
            _ => return RlExp::error("An arg to rlsub did not eval to an int"),
        },
        None => return RlExp::error("No args to rlsub"),
    };
    match args.len() {
        1 => return RlExp::int(0 - first_arg),
        _ => {
            let mut result = *first_arg;
            for exp in args.iter().skip(1) {
                match exp {
                    RlExp::Int(i) => result -= i,
                    RlExp::Error(_) => return exp.clone(),
                    _ => return RlExp::error("An arg to rlsub did not eval to an int"),
                }
            }

            return RlExp::int(result);
        }
    }
}

pub fn rlmul(args: &Vec<RlExp>) -> RlExp {
    let first_arg = match args.first() {
        Some(exp) => match exp {
            RlExp::Int(i) => i,
            RlExp::Error(_) => return exp.clone(),
            _ => return RlExp::error("An arg to rlmul did not eval to an int"),
        },
        None => return RlExp::error("No args to rlmul"),
    };
    match args.len() {
        1 => RlExp::int(*first_arg),
        _ => {
            let mut result = *first_arg;
            for exp in args.iter().skip(1) {
                match exp {
                    RlExp::Int(i) => result *= i,
                    RlExp::Error(_) => return exp.clone(),
                    _ => return RlExp::error("An arg to rlmul did not eval to an int"),
                }
            }

            return RlExp::int(result);
        }
    }
}

pub fn rldiv(args: &Vec<RlExp>) -> RlExp {
    let first_arg = match args.first() {
        Some(exp) => match exp {
            RlExp::Int(i) => i,
            RlExp::Error(_) => return exp.clone(),
            _ => return RlExp::error("An arg to rldiv did not eval to an int"),
        },
        None => return RlExp::error("No args to rldiv"),
    };
    match args.len() {
        1 => match first_arg {
            0 => return RlExp::error("div by 0 error"),
            1 => return RlExp::int(1),
            _ => return RlExp::int(0),
        },
        _ => {
            let mut result = *first_arg;
            for exp in args.iter().skip(1) {
                match exp {
                    RlExp::Int(i) => match i {
                        0 => return RlExp::error("div by 0 error"),
                        _ => result /= i,
                    },
                    RlExp::Error(_) => return exp.clone(),
                    _ => return RlExp::error("An arg to rldiv did not eval to an int"),
                }
            }

            return RlExp::int(result);
        }
    }
}

fn expr_<I>() -> impl Parser<Input = I, Output = RlExp>
where
    I: Stream<Item = char>,
    I::Error: ParseError<I::Item, I::Range, I::Position>,
{
    let skip_spaces = || spaces().silent();
    let lex_char = |c| char(c).skip(skip_spaces());

    let int = many1(digit()).map(|string: String| string.parse::<i64>().unwrap()); // Negatives / Decimals
    let sexpr = between(lex_char('('), lex_char(')'), sep_by(expr(), skip_spaces())); // qexpr?
    let sym = one_of("+-*/".chars()).map(|c: char| c.to_string()); // 4 ops? more general pattern needed

    choice((
        sym.map(|x| RlExp::sym(x)),
        int.map(|x| RlExp::int(x)),
        sexpr.map(|x| RlExp::sexpr(x)),
    ))
    .skip(skip_spaces())
}

parser! {
    fn expr[I]()(I) -> RlExp
    where [I: Stream<Item = char>]
    {
        expr_()
    }
}

fn parse(input: &str) -> RlExp {
    match expr().parse(input) {
        Ok(expr) => return expr.0,
        Err(_) => RlExp::error("Parse Error"),
    }
}

fn main() {
    // `()` can be used when no completer is required
    let mut rl = Editor::<()>::new();
    if rl.load_history("rl_history").is_err() {
        println!("No previous history.");
    }
    loop {
        let readline = rl.readline("rl> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                let parsed = parse(line.as_str());
                println!("{}", parsed.eval().to_string());
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
    rl.save_history("rl_history").unwrap();
}
